$(document).ready(function(){


  $('.owl-carousel').owlCarousel({
    loop: true,
    margin:20,
    navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
    responsiveClass:true,
    responsive:{
      0:{
        items:1,
        nav:true
      },
      600:{
        items:2,
        nav:true
      },
      850:{
        items:3,
        nav:true
      },
      1100:{
        items:4,
        nav:true,
        loop:false
      },
      1300:{
        items:5,
        nav:true,
        loop:false
      }
    }
  });


});
